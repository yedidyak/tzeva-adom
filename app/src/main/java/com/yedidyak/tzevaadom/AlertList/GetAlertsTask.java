package com.yedidyak.tzevaadom.AlertList;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.TextView;

import com.google.gson.Gson;
import com.yedidyak.tzevaadom.AppConstants;
import com.yedidyak.tzevaadom.R;
import com.yedidyak.tzevaadom.server.alerts.Alerts;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Yedidya on 13/07/2014.
 */
public class GetAlertsTask extends AsyncTask<Void, Void, List<Alert>> {

    private AlertItemAdapter adapter;
    private TextView txtLastUpdated;
    private Context context;

    public GetAlertsTask(AlertItemAdapter adapter, TextView txtLastUpdated, Context context) {
        this.adapter = adapter;
        this.txtLastUpdated = txtLastUpdated;
        this.context = context;
    }

    @Override
    protected List<Alert> doInBackground(Void... voids) {
        Alerts alertsEP = AppConstants.getAlertsApiServiceHandle();
        try{
            String alertsJSON = alertsEP.getAlerts().execute().getStr();

            List<Alert> alerts = new ArrayList<Alert>();

            Gson gson = new Gson();

            JSONArray alertsJSONArray = new JSONArray(alertsJSON);

            for (int i=0; i < alertsJSONArray.length(); i++){
                Alert alert = gson.fromJson(alertsJSONArray.getString(i), Alert.class);
                alerts.add(alert);
            }

            return alerts;


        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(List<Alert> alerts) {

        Date date = new Date();
        DateFormat tf = new SimpleDateFormat("HH:mm");
        String lastUpdate = context.getString(R.string.last_update);
        lastUpdate = lastUpdate + " " + tf.format(date);

        txtLastUpdated.setText(lastUpdate);

        if(alerts == null){
            alerts = new ArrayList<Alert>();
            Alert alert = new Alert();
            alert.locations = new HashSet<String>();
            alert.locations.add("No alerts in last 24 hrs!");
            alerts.add(alert);
        }

        this.adapter.setAlerts(alerts);
        this.adapter.notifyDataSetChanged();
    }
}
