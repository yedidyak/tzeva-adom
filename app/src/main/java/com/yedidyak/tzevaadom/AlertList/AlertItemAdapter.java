package com.yedidyak.tzevaadom.AlertList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.yedidyak.tzevaadom.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Yedidya on 13/07/2014.
 */
public class AlertItemAdapter extends BaseAdapter {

    private List<Alert> alerts = new ArrayList<Alert>();
    private Context mContext;

    public AlertItemAdapter(Context context) {
        this.mContext = context;
    }

    public void setAlerts(List<Alert> alerts) {
        this.alerts = alerts;
    }

    @Override
    public int getCount() {
        if(alerts == null){return 0;}
        return alerts.size();
    }

    @Override
    public Alert getItem(int i) {
        return alerts.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View newView = view;
        if(newView == null){
            newView = View.inflate(mContext, R.layout.alert_item, null);
        }

        Alert alert = getItem(i);

        String cities = "";

        for (String city : alert.locations){
            cities = cities + ", " + city;
        }

        cities = cities.substring(2);
        ((TextView) newView.findViewById(R.id.txtCities)).setText(cities);

        DateFormat tf = new SimpleDateFormat("HH:mm");
        ((TextView) newView.findViewById(R.id.txtTimeStamp)).setText(tf.format(new Date(alert.timestamp)));

        return newView;
    }
}
