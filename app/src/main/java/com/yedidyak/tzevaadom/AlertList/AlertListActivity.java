package com.yedidyak.tzevaadom.AlertList;

import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.yedidyak.tzevaadom.AboutActivity;
import com.yedidyak.tzevaadom.AppConstants;
import com.yedidyak.tzevaadom.NumberYishuvim.YishuvFinderActivity;
import com.yedidyak.tzevaadom.R;
import com.yedidyak.tzevaadom.RegisterTask;
import com.yedidyak.tzevaadom.SettingsActivity;

public class AlertListActivity extends ListActivity {

    private AlertItemAdapter adapter;
    private TextView txtLastUpdated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_list);
        SharedPreferences pref = getSharedPreferences("registered", MODE_APPEND);
        boolean registered = pref.getBoolean("registered", false);

        txtLastUpdated = (TextView)findViewById(R.id.txtLastUpdate);

        if(!registered){
            new RegisterTask(this).execute();
        }

        adapter = new AlertItemAdapter(this);

        setListAdapter(adapter);

        ProgressBar pb = new ProgressBar(this);
        pb.setIndeterminate(true);

        getListView().setEmptyView(pb);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.alert_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent i;
        switch (id){
            case R.id.action_settings:
                i = new Intent(this, SettingsActivity.class);
                startActivity(i);
                return true;
            case R.id.action_about:
                i = new Intent(this, AboutActivity.class);
                startActivity(i);
                return true;
            case R.id.action_register:
                new RegisterTask(this).execute();
            case R.id.action_refresh:
                adapter.setAlerts(null);
                adapter.notifyDataSetChanged();
                new GetAlertsTask(adapter, txtLastUpdated, this).execute();
                return true;
            case R.id.action_where_am:
                i = new Intent(this, YishuvFinderActivity.class);
                startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            new GetAlertsTask(adapter, txtLastUpdated, getApplicationContext()).execute();
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetAlertsTask(adapter, txtLastUpdated, this).execute();
        IntentFilter filter = new IntentFilter();
        filter.addAction(AppConstants.BROADCAST_UPDATE);
        registerReceiver(receiver, filter);
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);  // Add this method.
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);  // Add this method.
    }
}
