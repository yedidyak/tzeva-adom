package com.yedidyak.tzevaadom;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.yedidyak.tzevaadom.server.alerts.Alerts;
import com.yedidyak.tzevaadom.server.registration.Registration;

/**
 * Created by Yedidya on 08/07/2014.
 */
public class AppConstants {

    public static final String BROADCAST_UPDATE = "com.yedidyak.tzevaadom.UPDATE";

    public static final JsonFactory JSON_FACTORY = new AndroidJsonFactory();
    public static final HttpTransport HTTP_TRANSPORT = AndroidHttp.newCompatibleTransport();


    public static Registration getRegistrationApiServiceHandle() {
        Registration.Builder regEP = new Registration.Builder(AppConstants.HTTP_TRANSPORT,
                AppConstants.JSON_FACTORY,null).setApplicationName("tzeva-adom");

        return regEP.build();
    }

    public static Alerts getAlertsApiServiceHandle() {
        Alerts.Builder alertsEP = new Alerts.Builder(AppConstants.HTTP_TRANSPORT,
                AppConstants.JSON_FACTORY,null).setApplicationName("tzeva-adom");

        return alertsEP.build();
    }

}
