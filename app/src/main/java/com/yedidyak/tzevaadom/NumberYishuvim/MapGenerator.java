package com.yedidyak.tzevaadom.NumberYishuvim;

import android.content.Context;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimaps;
import com.yedidyak.tzevaadom.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Yedidya on 14/07/2014.
 */
public class MapGenerator {


    private static MapGenerator ourInstance = null;

    public static MapGenerator getInstance(Context context) {

        if(ourInstance == null){
            ourInstance = new MapGenerator(context);
        }

        return ourInstance;
    }

    ArrayListMultimap<String, String> multimap;

    private MapGenerator(Context context) {

        this.multimap = getMap(context);

    }

    private static ArrayListMultimap<String, String> getMap(Context context){

        ArrayListMultimap<String, String> multimap = ArrayListMultimap.create();

        InputStream inputStream = context.getResources().openRawResource(R.raw.names);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        String line;

        try {
            while ((line = reader.readLine()) != null) {

                String[] values = line.split(",");
                multimap.put(values[0], values[1]);

            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return multimap;
    }


    public ArrayListMultimap<String, String> getRegionKeyMM(Context context){

        ArrayListMultimap<String, String> reversedMap = ArrayListMultimap.create();
        Multimaps.invertFrom(multimap, reversedMap);

        return reversedMap;
    }

    public ArrayListMultimap<String, String> getYishuvKeyMM(Context context){

        return multimap;
    }


}
