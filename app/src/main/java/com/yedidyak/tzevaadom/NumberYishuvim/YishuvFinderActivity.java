package com.yedidyak.tzevaadom.NumberYishuvim;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.yedidyak.tzevaadom.R;

import java.util.List;
import java.util.Set;

public class YishuvFinderActivity extends ActionBarActivity {

    private MapGenerator gen;
    private AutoCompleteTextView txtYishuv;
    private TextView txtResolvedRegion;
    private AutoCompleteTextView txtRegion;
    private TextView txtResolvedYishuv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yishuv_finder);

        gen = MapGenerator.getInstance(this);

        txtYishuv = (AutoCompleteTextView)findViewById(R.id.txtYishuv);
        txtResolvedRegion = (TextView)findViewById(R.id.txtResolvedNumber);
        txtRegion = (AutoCompleteTextView)findViewById(R.id.txtRegion);
        txtResolvedYishuv = (TextView)findViewById(R.id.txtResolvedYishuv);

        Set<String> yishuvSet = gen.getYishuvKeyMM(this).keySet();
        String[] yishuvArr = yishuvSet.toArray(new String[yishuvSet.size()]);

        ArrayAdapter<String> yishuvAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, yishuvArr);

        txtYishuv.setAdapter(yishuvAdapter);

        Set<String> regionSet = gen.getRegionKeyMM(this).keySet();
        String[] regionArr = regionSet.toArray(new String[regionSet.size()]);

        ArrayAdapter<String> regionAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, regionArr);

        txtRegion.setAdapter(regionAdapter);

        txtYishuv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                resolveYishuv();
            }
        });

        txtYishuv.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                resolveYishuv();
                return true;
            }
        });

        txtRegion.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                resolveRegion();
            }
        });

        txtRegion.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                resolveRegion();
                return true;
            }
        });

    }

    private void resolveYishuv(){
        String yishuv = txtYishuv.getText().toString();

        List<String> listRegions = gen.getYishuvKeyMM(getApplicationContext()).get(yishuv);

        String regionCode;

        if(listRegions.size() == 0){
            regionCode = "Invalid";
        }
        else{
            regionCode = listRegions.get(0);
        }

        txtResolvedRegion.setText(regionCode);
    }

    private void resolveRegion(){
        String region = txtRegion.getText().toString();

        List<String> yishuvList = gen.getRegionKeyMM(getApplicationContext()).get(region);

        String yishuvim = "";

        if(yishuvList.size()==0){
            yishuvim = "Invalid";
        }

        else{
            for (String yishuv : yishuvList){
                yishuvim = yishuvim + ", " + yishuv;
            }
            yishuvim = yishuvim.substring(2);
        }



        txtResolvedYishuv.setText(yishuvim);
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);  // Add this method.
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);  // Add this method.
    }

}
