package com.yedidyak.tzevaadom.GCMHandlers;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.Tracker;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.yedidyak.tzevaadom.AlertList.AlertListActivity;
import com.yedidyak.tzevaadom.AppConstants;
import com.yedidyak.tzevaadom.R;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.util.HashSet;
import java.util.Set;

public class GCMReceiverService extends IntentService {

    public GCMReceiverService() {
        super("GCMReceiverService");
    }

    private NotificationManager mNotificationManager;

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                Log.d("TA", "GOT A MESSAGE!");
                Log.d("TA", "Message is: " + extras.toString());

            }
        }

        try {

            String cities = extras.getString("message");
            cities = URLDecoder.decode(cities, "UTF-8");

            SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(this);
            boolean showNotif = preference.getBoolean("notifications_new_message", true);

            boolean showAllLocs = preference.getBoolean("all_locations", true);

            //TODO - replace all non-numeric chars by regex ("[^0-9]+", " ")
            //TODO - split by ' '
            //TODO - compare numbers using .equals()


            if(!showAllLocs) {

                showNotif = false;
                String replacedStr = cities.replaceAll("[^0-9]+", " ");
                String[] splitCities = replacedStr.split(" ");
                Set<String> locsToShow = preference.getStringSet("list_locations", new HashSet<String>());

                for (int i = 0; i < splitCities.length; i++) {

                    if(showNotif){break;}

                    for (String loc : locsToShow) {
                        if(splitCities[i].equals("")){continue;}
                        if (loc.equals(splitCities[i])){
                            showNotif = true;
                            break;
                        }
                    }
                }
            }

            /*if(!showAllLocs){
                showNotif = false;
                Set<String> locsToShow = preference.getStringSet("list_locations", new HashSet<String>());
                for (String loc : locsToShow){
                    if(cities.contains(loc)){
                        //Toast.makeText(this, "FOUND LOCATION " + loc, Toast.LENGTH_LONG);
                        showNotif = true;
                        break;
                    }
                }
            }*/

            if(showNotif) {

                sendTANotification(cities);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        Intent broadcast = new Intent();
        broadcast.setAction(AppConstants.BROADCAST_UPDATE);
        sendBroadcast(broadcast);

        GCMBroadcastReceiver.completeWakefulIntent(intent);
    }


    private void sendTANotification(String cities){

        int id = getSharedPreferences("notif", MODE_APPEND).getInt("notif", 1);
        id++;
        getSharedPreferences("notif", MODE_APPEND).edit().putInt("notif", id).commit();

        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent i = new Intent(this, AlertListActivity.class);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, i, PendingIntent.FLAG_CANCEL_CURRENT);


        SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(this);
        String strRingtonePreference = preference.getString("notifications_new_message_ringtone", "DEFAULT_SOUND");
        Uri soundUri = Uri.parse(strRingtonePreference);

        //Uri soundUri = Uri.parse("android.resource://com.yedidyak.tzevaadom/" + R.raw.done);

        boolean vibrate = preference.getBoolean("notifications_new_message_vibrate", true);

        String tzevaAdom = getString(R.string.tzeva_adom);

        String colorString = preference.getString("notification_light", "#ffff0000");
        String lightSpeedString = preference.getString("notification_speed", "fast");

        int color = Color.parseColor(colorString);

        int lightSpeed = 100;
        if(lightSpeedString.equals("fast")){lightSpeed = 100;}
        else if(lightSpeedString.equals("medium")){lightSpeed = 400;}
        else if(lightSpeedString.equals("slow")){lightSpeed = 800;}

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSound(soundUri)
                        .setContentIntent(contentIntent)
                        .setAutoCancel(true)
                        .setContentText(cities)
                        .setTicker(tzevaAdom + ": " + cities)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(cities))
                        .setLights(color, lightSpeed, lightSpeed)
                        .setSmallIcon(R.drawable.ic_stat_warning_icon)
                        .setContentTitle(tzevaAdom);

        if(vibrate){
            mBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
        }

        Notification notification = mBuilder.build();
        //notification.ledARGB = 0xffff0000;
        //notification.flags = Notification.FLAG_SHOW_LIGHTS

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(id, notification);

    }


}