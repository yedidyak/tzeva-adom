package com.yedidyak.tzevaadom;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.yedidyak.tzevaadom.server.registration.Registration;

import java.io.IOException;

/**
 * Created by Yedidya on 08/07/2014.
 */
public class RegisterTask extends AsyncTask<Void, Void, Boolean> {

    private Context mContext;
    private ProgressDialog progressDialog;

    public RegisterTask(Context context) {
        this.mContext = context;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        try {
            Registration r = AppConstants.getRegistrationApiServiceHandle();
            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(mContext);
            String regID = gcm.register("50013985461");

            r.register(regID).execute();

            return true;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setTitle("Registering");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected void onPostExecute(Boolean success) {

        progressDialog.hide();

        if (success){
            SharedPreferences pref = mContext.getSharedPreferences("registered", Context.MODE_APPEND);
            pref.edit().putBoolean("registered", true).commit();
            Toast.makeText(mContext, mContext.getResources().getString(R.string.registration_success) , Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(mContext, mContext.getResources().getString(R.string.registration_success) , Toast.LENGTH_LONG).show();
        }
    }
}
