package com.yedidyak.tzevaadom.server;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

/**
 * Created by Yedidya on 09/07/2014.
 */

@Entity
@Cache
public class RegID {

    @Id String regID;

}
