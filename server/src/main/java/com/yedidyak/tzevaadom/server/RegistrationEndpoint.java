package com.yedidyak.tzevaadom.server;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.inject.Named;

/**
 * A registration endpoint class we are exposing for a device's GCM registration id on the backend
 *
 * For more information, see
 * https://developers.google.com/appengine/docs/java/endpoints/
 *
 * NOTE: This endpoint does not use any form of authorization or
 * authentication! If this app is deployed, anyone can access this endpoint! If
 * you'd like to add authentication, take a look at the documentation.
 */
@Api(name = "registration", version = "v1", namespace = @ApiNamespace(ownerDomain = "server.tzevaadom.yedidyak.com", ownerName = "server.tzevaadom.yedidyak.com", packagePath=""))
public class RegistrationEndpoint {

    private static final Logger log = Logger.getLogger(RegistrationEndpoint.class.getName());

    /**
     * Register a device to the backend
     *
     * @param regId The Google Cloud Messaging registration Id to add
     */
    @ApiMethod(name = "register")
    public void registerDevice(@Named("regId") String regId) {
        if(findRecord(regId) != null) {
            log.info("Device " + regId + " already registered, skipping register");
            return;
        }
        RegistrationRecord record = new RegistrationRecord();
        record.setRegId(regId);


        RegID regID = new RegID();
        regID.regID = regId;

        Stats stats = ObjectifyService.ofy().load().key(Key.create(Stats.class, "stats")).now();
        stats.incUsers();

        OfyService.ofy().save().entities(record, regID, stats).now();
        ObjectifyService.ofy().load().entity(stats).now();

    }

    /**
     * Unregister a device from the backend
     *
     * @param regId The Google Cloud Messaging registration Id to remove
     */
    @ApiMethod(name = "unregister")
    public void unregisterDevice(@Named("regId") String regId) {
        RegistrationRecord record = findRecord(regId);
        if(record == null) {
            log.info("Device " + regId + " not registered, skipping unregister");
            return;
        }
        OfyService.ofy().delete().entity(record).now();
    }

    /**
     * Return a collection of registered devices
     *
     * @param count The number of devices to list
     * @return a list of Google Cloud Messaging registration Ids
     */
    @ApiMethod(name = "listDevices")
    public CollectionResponse<RegistrationRecord> listDevices(@Named("count") int count) {
        List<RegistrationRecord> records = OfyService.ofy().load().type(RegistrationRecord.class).limit(count).list();
        return CollectionResponse.<RegistrationRecord>builder().setItems(records).build();
    }

    private RegistrationRecord findRecord(String regId) {
        return OfyService.ofy().load().type(RegistrationRecord.class).filter("regId", regId).first().now();
    }

    @ApiMethod(name = "swapAll")
    public void swapAll(){
        List<RegistrationRecord> records = ObjectifyService.ofy().load().type(RegistrationRecord.class).list();
        List<RegID> regIDs = new ArrayList<RegID>();

        log.warning("Got " + records.size() + " records.");

        for(RegistrationRecord record : records){
            RegID regID = new RegID();
            regID.regID = record.getRegId();
            regIDs.add(regID);

            //log.warning("Changed a record!");
        }

        log.warning("Made " + regIDs.size() + " new records");

        ObjectifyService.ofy().save().entities(regIDs).now();
        log.warning("Changed and saved all records.");
    }

}