package com.yedidyak.tzevaadom.server;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.gson.Gson;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Logger;

import static com.yedidyak.tzevaadom.server.OfyService.ofy;

/**
 * Created by Yedidya on 13/07/2014.
 */

@Api(name = "alerts", version = "v1", namespace = @ApiNamespace(ownerDomain = "server.tzevaadom.yedidyak.com", ownerName = "server.tzevaadom.yedidyak.com", packagePath=""))

public class AlertEndpoint {


    private static final Logger log = Logger.getLogger(AlertEndpoint.class.getName());

    @ApiMethod(name = "getAlerts")
    public StringWrapper getAlerts(){

        List<Alert> alerts = new ArrayList<Alert>();

        Calendar cal = new GregorianCalendar();
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        long now_time = cal.getTimeInMillis();

        cal.add(Calendar.DAY_OF_YEAR, -1);
        long yesterday = cal.getTimeInMillis();

        //log.warning("Searching from " + yesterday + " to " + now_time);

        Key<Alert> startKey = Key.create(Alert.class, yesterday);
        Key<Alert> endKey = Key.create(Alert.class, now_time);

        alerts = ofy().load()
                    .type(Alert.class)
                    .filterKey(">=", startKey)
                    .filterKey("<=", endKey)
                    .order("-timestamp")
                .list();

        log.info("Got " + alerts.size() + " alerts");

        JSONArray jsonArray = new JSONArray();
        Gson gson = new Gson();

        for (Alert alert : alerts){
            jsonArray.put(gson.toJson(alert));
        }

        //log.warning("JSON: " + jsonArray.toString());

        return new StringWrapper(jsonArray.toString());

    }

}
