package com.yedidyak.tzevaadom.server;

import com.google.android.gcm.server.Constants;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.google.appengine.api.taskqueue.TaskOptions.Builder.withUrl;

/**
 * Created by Yedidya on 08/07/2014.
 */
public class RecurringTask extends HttpServlet{

    final static int CONNECTION_TIMEOUT = 1000;
    final static int CONNECTION_RETRY = 20;
    final static int POLL_INTERVAL = 2000;
    final static String JSON_URL = "http://pcpower.co.il/nle/4270143126.php?access=y9314847908";

    private static final Logger log = Logger.getLogger(RecurringTask.class.getName());

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String lastID = req.getParameter("lastID");

        String jsonAlert = "";


        try {
            URL url = new URL(JSON_URL);
            URLConnection connection = url.openConnection();
            connection.setRequestProperty("Cache-Control", "max-age=10");
            connection.setConnectTimeout(CONNECTION_TIMEOUT);
            InputStream is = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-16LE")));
            jsonAlert = readAll(reader);
            //log.warning("Original JSON: " + jsonAlert);
        }
        catch (SocketTimeoutException e){

            log.severe("Socket Timeout, retrying!");
            Queue queue = QueueFactory.getDefaultQueue();
            queue.add(withUrl("/recurringTask").param("lastID", lastID).countdownMillis(CONNECTION_RETRY).method(TaskOptions.Method.GET));
            resp.getWriter().println("");
            return;
        }
        catch (IOException e2){
            log.severe("IO Exception, retrying!");
            Queue queue = QueueFactory.getDefaultQueue();
            queue.add(withUrl("/recurringTask").param("lastID", lastID).countdownMillis(CONNECTION_RETRY).method(TaskOptions.Method.GET));
            resp.getWriter().println("");
            return;
        }

        Gson gson = new Gson();

        try{

            jsonAlert = jsonAlert.substring(jsonAlert.indexOf('{'));
            jsonAlert = jsonAlert.replaceAll("\u0000", "");
            jsonAlert = jsonAlert.replaceAll("\r", "");
            jsonAlert = jsonAlert.substring(0, jsonAlert.indexOf('}'));
            jsonAlert = jsonAlert + "}";
        }
        catch (Exception e){

            log.warning("Error reading JSON");
            //Queue queue = QueueFactory.getDefaultQueue();
            //queue.add(withUrl("/recurringTask").param("lastID", lastID).countdownMillis(POLL_INTERVAL).method(TaskOptions.Method.GET));
            //return;
        }

        //log.warning("fixed JSON is : " + jsonAlert);

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonAlert);
            //log.warning("JSON: " + jsonObject.toString());

            if(!lastID.equals(jsonObject.getString("id")) && !lastID.equals("0")){

                //log.warning("Change in alert JSON!");

                lastID = jsonObject.getString("id");
                log.info("Set lastID to " + lastID);

                JSONArray cities = jsonObject.getJSONArray("data");
                String cityList = "";

                for (int i=0; i < cities.length(); i++){
                    cityList = cityList + cities.getString(i) + ", ";
                }

                cityList = cityList.substring(0, cityList.length()-2);

                if(!cityList.trim().equals("")){

                    sendMessage(cityList);


                    log.warning("NEW ALERT! :(    " + cityList);

                    //SAVE ALERT
                    Date date = new Date();

                    Calendar calendar = new GregorianCalendar();
                    calendar.set(Calendar.SECOND, 0);
                    calendar.set(Calendar.MILLISECOND, 0);

                    long timeStamp =  calendar.getTimeInMillis();

                    Key<Alert> alertKey = Key.create(Alert.class, timeStamp);
                    //log.warning("Made Key for Alert, timestamp = " + timeStamp);
                    Alert alert = ObjectifyService.ofy().load().key(alertKey).now();

                    //log.warning("Alert = " + alert);

                    if (alert == null){
                        alert = new Alert(timeStamp);
                        log.info("Creating new alert at " + timeStamp);
                    }

                    for (int i=0; i < cities.length(); i++){
                        //log.warning("Adding city... " + cities.getString(i));
                        alert.addLocation(cities.getString(i));
                    }

                    ObjectifyService.ofy().save().entity(alert).now();
                    //log.warning("Saving alert");

                }
            }

            else{
                //log.warning("No new alerts :)");
                lastID = jsonObject.getString("id");
            }

            lastID = jsonObject.getString("id");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Queue queue = QueueFactory.getDefaultQueue();
        queue.add(withUrl("/recurringTask").param("lastID", lastID).countdownMillis(POLL_INTERVAL).method(TaskOptions.Method.GET));

        resp.getWriter().println("");

    }

    static <T> List<List<T>> chopped(List<T> list, final int L) {
        List<List<T>> parts = new ArrayList<List<T>>();
        final int N = list.size();
        for (int i = 0; i < N; i += L) {
            parts.add(new ArrayList<T>(
                            list.subList(i, Math.min(N, i + L)))
            );
        }
        return parts;
    }



    private static final String API_KEY = System.getProperty("gcm.api.key");


    public void sendMessage(String message) throws IOException {


        Stats stats = ObjectifyService.ofy().load().key(Key.create(Stats.class, "stats")).now();

        if(message == null || message.trim().length() == 0) {
            return;
        }
        // crop longer messages
        if (message.length() > 1000) {
            message = message.substring(0, 1000) + "[...]";
        }

        Sender sender = new Sender(API_KEY);
        Message msg = new Message.Builder().addData("message", URLEncoder.encode(message, "UTF-8")).build();


        List<Key<RegID>> regKeys = ObjectifyService.ofy().load().type(RegID.class).keys().list();
        List<String> ids = new ArrayList<String>();

        for(Key<RegID> regIDKey : regKeys){
            ids.add(regIDKey.getName());
        }

        log.warning("Got " + ids.size() + " regIds");


        ///split list into chunks of 500
        List<List<String>> listOfLists = chopped(ids, 500);

        //log.warning("Split into " + listOfLists.size() + " chunks of 500");

        List<MulticastResult> results = new ArrayList<MulticastResult>();

        for (List<String> idList : listOfLists){
            //log.warning("Chunk size " + idList.size());
            MulticastResult result = sender.send(msg, idList, 5);
            results.add(result);
        }

        //log.warning("Sent to all chunks");


        int j=0;

        int updated = 0;
        int deleted = 0;

        for (MulticastResult result : results){

            for (int i = 0; i < result.getTotal(); i++) {
                Result r = result.getResults().get(i);

                if (r.getMessageId() != null) {
                    String canonicalRegId = r.getCanonicalRegistrationId();
                    if (canonicalRegId != null) {

                        String newRegIDVal = canonicalRegId;
                        //Key<RegID> oldKey = regKeys.get(i);
                        String oldVal = listOfLists.get(j).get(i);
                        Key<RegID> oldKey = Key.create(RegID.class, oldVal);

                        ObjectifyService.ofy().delete().key(oldKey);
                        RegID newRegID = new RegID();
                        newRegID.regID = newRegIDVal;
                        ObjectifyService.ofy().save().entity(newRegID);
                        updated++;

                    }
                } else {
                    String error = r.getErrorCodeName();
                    if (error.equals(Constants.ERROR_NOT_REGISTERED)) {

                        //ObjectifyService.ofy().delete().key(regKeys.get(i));
                        //deleted++;
                        //stats.decUsers();
                    }
                }
            }
            j++;
        }

        log.warning("Updated: " + updated);
        //log.warning("Deleted: " + deleted);

        stats.incAlerts();


        ObjectifyService.ofy().save().entity(stats).now();
        ObjectifyService.ofy().load().entity(stats).now();


        //log.warning("Checked all regIDs");

    }
}
