package com.yedidyak.tzevaadom.server;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

/**
 * Created by Yedidya on 11/07/2014.
 */

@Entity
@Cache
public class Stats {

    @Id String id;

    public int num_users;
    public int alerts_sent;

    public void incUsers(){
        num_users++;
    }

    public void decUsers(){
        num_users--;
    }

    public void incAlerts(){
        alerts_sent++;
    }
}
