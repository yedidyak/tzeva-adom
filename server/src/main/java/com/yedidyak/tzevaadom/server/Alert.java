package com.yedidyak.tzevaadom.server;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Yedidya on 13/07/2014.
 */

@Entity
@Cache
public class Alert {

    public Alert(long timestamp){
        this.timestamp = timestamp;
    }

    public Alert(){}

    @Id
    @Index long timestamp;
    HashSet<String> locations = new HashSet<String>();
    public void addLocation(String location){
        locations.add(location);
    }

}
