package com.yedidyak.tzevaadom.server;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

import java.io.IOException;
import java.util.List;

import static com.google.appengine.api.taskqueue.TaskOptions.Builder.withUrl;

/**
 * Created by Yedidya on 08/07/2014.
 */

@Api(name = "admin", version = "v1", namespace = @ApiNamespace(ownerDomain = "server.tzevaadom.yedidyak.com", ownerName = "server.tzevaadom.yedidyak.com", packagePath=""))

public class Admin {

    @ApiMethod(name = "start")
    public void start(){

        Queue queue = QueueFactory.getDefaultQueue();
        queue.add(withUrl("/recurringTask").param("lastID", "0").countdownMillis(2000).method(TaskOptions.Method.GET));

    }

    @ApiMethod(name = "stop")
    public void stop(){

        Queue queue = QueueFactory.getDefaultQueue();
        queue.purge();

    }

    @ApiMethod(name = "resetStats")
    public void resetStats(){
        Stats stats = new Stats();
        stats.id = "stats";

        List<Key<RegID>> regKeys = ObjectifyService.ofy().load().type(RegID.class).keys().list();
        stats.num_users = regKeys.size();

        ObjectifyService.ofy().save().entity(stats).now();
    }

    @ApiMethod(name = "sendMessage")
    public void sendMessage(@Named("message") String message){
        try {
            new RecurringTask().sendMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
