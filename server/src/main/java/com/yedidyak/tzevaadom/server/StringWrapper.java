package com.yedidyak.tzevaadom.server;

/**
 * Created by Yedidya on 13/07/2014.
 */
public class StringWrapper {

    public StringWrapper(String str){
        this.str = str;
    }

    String str;

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }
}
